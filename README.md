# JSdifflib Legacy

A javascript library for diffing text and generating corresponding HTML views.

Original library here : https://github.com/cemerick/jsdifflib/.

Merged with some PR of the original github content.

## Usage
There is three files located in assets/ :
- js/difflib.js
- js/diffview.js
- css/diffview.js

Use these files directly or point a virtualhost index to public/.

## Interface example
![JSdifflib Legacy](https://gitlab.com/fredericpetit/jsdifflib-legacy/-/raw/main/assets/img/interface.png)