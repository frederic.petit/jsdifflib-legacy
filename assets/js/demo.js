//############################                                                              
//# last revision : 26/07/2022
//############################
//# script : demo.js
//# usage : demo js
//############################

document.getElementsByName('_viewtype').checked = false;

function diffUsingJS() {

	var byId = function (id) { return document.getElementById(id); };

    // get the baseText and newText values from the two textboxes, and split them into lines
    var base = difflib.stringAsLines(byId("baseText").value);
    var newtxt = difflib.stringAsLines(byId("newText").value);

    // create a SequenceMatcher instance that diffs the two sets of lines
    var sm = new difflib.SequenceMatcher(base, newtxt);

    // get the opcodes from the SequenceMatcher instance
    // opcodes is a list of 3-tuples describing what changes should be made to the base text
    // in order to yield the new text
    var opcodes = sm.get_opcodes();
    var diffoutputdiv = byId("diffoutput");
    while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
    var contextSize = byId("contextSize").value;
    contextSize = contextSize ? contextSize : null;

    // build the diff view and add it to the current DOM
    diffoutputdiv.appendChild(diffview.buildView({
        baseTextLines: base,
        newTextLines: newtxt,
        opcodes: opcodes,
        // set the display titles for each resource
        baseTextName: "Base Text",
        newTextName: "New Text",
        contextSize: contextSize,
        viewType: byId("inline").checked ? 1 : 0
    }));

}